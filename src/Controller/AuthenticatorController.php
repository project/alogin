<?php

namespace Drupal\alogin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AuthenticatorController extends ControllerBase {
  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @param \Drupal\Core\Routing\RouteMatchInterface $account
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match')
    );
  }

  public function access(AccountInterface $account) {
    $user_object_from_route = $this->routeMatch->getParameter('user');
    $route_user_id = NULL;

    if (gettype($user_object_from_route) == 'object') {
      $route_user_id = $user_object_from_route->id();
    }

    return AccessResult::allowedIf($account->isAuthenticated() && $route_user_id === $account->id());
  }
}
